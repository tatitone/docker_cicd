FROM openjdk:8-jdk
COPY . /
RUN ./mvnw --batch-mode package

FROM openjdk:8-jdk
COPY --from=0 target/*jar app.jar
ENV SERVER_PORT=8088
ENTRYPOINT ["java", "-Djava.security.egd=filer:/dev/./urandom", "-jar", "/app.jar"]
EXPOSE 8088


# COMMENTAIRE
# --batch-mode : The batch-mode will automatically use default values instead of asking you via prompt for those values
#"-Djava.security.egd=filer:/dev/./urandom" : If running Java 8 on modern OSes with support to Deterministic Random Bit Generator (DRBG)